'''
+-------------------------+-------------+
|Measurable                |            |
+-------------------------+-------------+
|compressor_air_oil_temp   | PLC        |
|compressor_air_oil_press  | PLC        |
|discharge_pressure        | PLC        |
|upstream_pressure         | DPT        |
|downstream_pressure       | DPT        |
|current                   | MFM        |
|voltage                   | MFM        |
|power                     | MFM        |
|ambient_temp              | Sensor     |
|ambient_humidity          | Sensor     |
|ambient_pressure          | Google     |
+---------------------------------------+
'''
