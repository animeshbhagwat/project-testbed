import openpyxl
import os


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def create_new_excel(name):
    #Create temp excel and pdf files for viewing
    os.system("cp template_pdf/R2_parameters.xlsx temp.xlsx")
    os.system("soffice --headless --convert-to pdf temp.xlsx")
    #Actual files in Excel and PDF directories
    os.system("cp template_pdf/R2_parameters.xlsx reports/Excel/"+name+".xlsx")
    os.system("cp temp.pdf reports/PDF .")
    os.system("mv reports/PDF/temp.pdf reports/PDF/"+name+".pdf")

def update_basic_details(basic_details,name="R2_parameters.xlsx"):
    #UPDATE EXCEL
    book = openpyxl.load_workbook(name)
    sheet = book["Internal"]
    # sheet['A10']="Auto-Generated Report"
    
    sheet['A5'] = "Compressor Model : "+basic_details["compressor_model"]
    sheet['C5'] = ":"+str(basic_details["compressor_serial_no"])
    sheet['A6'] = "Motor Make : " + basic_details["motor_make"]+"  "+"KW :"+basic_details["motor_kw"]
    sheet['C6']=":" + str(basic_details["motor_srno"])
    sheet['A7']="Motor RPM : "+basic_details["motor_rpm"]+"   "+"Eff. :"+basic_details["motor_eff"]

    sheet['A8'] = "Fan Motor Make : " + basic_details["fan_motor_make"] + "  " + "KW :" + basic_details["fan_motor_kw"]
    sheet['C8'] = ":"+str(basic_details["fan_motor_srno"])
    sheet['A9'] = "Motor RPM : " + basic_details["fan_motor_rpm"] + "   " + "Eff. :" + basic_details["fan_motor_eff"]

    sheet['E5'] = "OA Number: "+str(basic_details["oa_number"])
    sheet['E6'] = "SBM Number: "+str(basic_details["sbm_number"])

    sheet["B45"]=""+str(basic_details["assembly_incharge"])
    sheet["E45"]=""+str(basic_details["qa_incharge"])

    if isfloat(basic_details["ambient_pressure"]):
        sheet["C13"]=round(float(basic_details["ambient_pressure"]),1)
        sheet["D13"]=round(float(basic_details["ambient_pressure"]),1)
        sheet["E13"] = round(float(basic_details["ambient_pressure"]),1)
        sheet["F13"] = round(float(basic_details["ambient_pressure"]), 1)

    if isfloat(basic_details["ambient_temperature"]):
        sheet["C14"] = round(float(basic_details["ambient_temperature"]),1)
        sheet["D14"] = round(float(basic_details["ambient_temperature"]),1)
        sheet["E14"] = round(float(basic_details["ambient_temperature"]),1)
        sheet["F14"] = round(float(basic_details["ambient_temperature"]),1)

    if isfloat(basic_details["suction_drop"]):
        sheet["C27"] = round(float(basic_details["suction_drop"]),1)
        sheet["D27"] = round(float(basic_details["suction_drop"]),1)
        sheet["E27"] = round(float(basic_details["suction_drop"]),1)
        sheet["F27"] = round(float(basic_details["suction_drop"]), 1)

    if isfloat(basic_details["actual_operating_pressure"]):
        sheet["D15"] = round(float(basic_details["actual_operating_pressure"]), 1)
        sheet["E15"] = round(float(basic_details["actual_operating_pressure"]), 1)
        sheet["C15"] = round(float(basic_details["actual_operating_pressure"]), 1)
        sheet["F15"] = round(float(basic_details["actual_operating_pressure"]), 1)

    if isfloat(basic_details["actual_RPM"]):
        sheet["C22"] = round(float(basic_details["actual_RPM"]),1)
        sheet["D22"] = round(float(basic_details["actual_RPM"]), 1)
        sheet["E22"] = round(float(basic_details["actual_RPM"]), 1)
        sheet["F22"] = round(float(basic_details["actual_RPM"]), 1)

    sheet["C23"] = round(float(32.0), 1)
    sheet["D23"] = round(float(32.2), 1)
    sheet["E23"] = round(float(32.3), 1)
    sheet["F23"] = round(float(31.5), 1)

    sheet["A36"] = "Motor Output Power in KW corrected at "+str(basic_details["actual_RPM"]) +" RPM"
    sheet["A30"] ="FAD Corrected with " +str(basic_details["actual_RPM"])+ " RPM & Condensate content in m3/min."

    sheet['E7'] = "Date of Test: " + str(basic_details["test_date"])
    sheet['E8'] = "Test Started: " + str(basic_details["test_start_time"])
    sheet['E9'] = "Test Stopped: " + str(basic_details["test_stop_time"])
    book.save(name)
    book.close()


def update_model_details(model_details,name="R2_parameters.xlsx"):
    # UPDATE EXCEL
    book = openpyxl.load_workbook(name)
    sheet = book["Internal"]
    if model_details["item_count"] == 1:
        sheet["A42"] = "Nozzle dia.:" + str(model_details['items'][0]["Nozzle Size"])
        if isfloat(model_details['items'][0]["Safety Valve Pressure"]):
            sheet["C37"] = round(float(model_details['items'][0]["Safety Valve Pressure"]), 1)
        if isfloat(str(model_details['items'][0]["Std  FAD in m3/min"])):
            sheet["B28"] =  str(model_details['items'][0]['Std  FAD in m3/min'])
        if isfloat(str(model_details['items'][0][" Std FAD in CFM"])):
            sheet["B31"] =  str(model_details['items'][0][' Std FAD in CFM'])
        if isfloat(str(model_details['items'][0]["Loading Pressure"])):
            sheet["C38"] = round(float(model_details['items'][0]["Loading Pressure"]), 1)
        if isfloat(str(model_details['items'][0]["Unloading Pressure"])):
            sheet["C39"] = round(float(model_details['items'][0]["Unloading Pressure"]), 1)

    book.save(name)
    book.close()







#----------------------------------------------------------Created and Edited by Anirudh_6_oct_2020---------------------------------------------------------------------------------------------------


def update_basic_details_auto(basic_details,name="final_format.xlsx"):
    #UPDATE EXCEL
    book = openpyxl.load_workbook(name)
    sheet = book["Internal"]
    # sheet['A10']="Auto-Generated Report"
    sheet['B5'] = str("Compressor Model : "+str(basic_details["Software"]["Compressor Model"]))
    sheet['D5'] = ":"+str(basic_details["Software"]["Compressor Sr No"])
    sheet['G5'] = ":"+str(basic_details["Software"]["OA Number"])
    sheet['B6'] = "Motor Make : " + str(basic_details["Software"]["Motor"]["Make"]+"  "+"KW :"+basic_details["Software"]["Motor"]["KW"])
    sheet['D6']=":" + str(basic_details["Software"]["Motor"]["Serial Number"])
    sheet['G6'] = ":"+str(basic_details["Software"]["SBM Number"])
    sheet['B7']="Motor RPM : "+str(basic_details["Software"]["Motor"]["RPM"]+"   "+"Efficiency :"+basic_details["Software"]["Motor"]["Efficiency"])+" %"
    sheet['G7'] = ":"+str(basic_details["Software"]["Test Date"])
    sheet['B8'] = "Fan Motor Make : " + str(basic_details["Software"]["Fan Motor"]["Make"] + "  " + "KW :" + basic_details["Software"]["Fan Motor"]["KW"])
    sheet['D8'] = str(basic_details["Software"]["Fan Motor"]["Serial Number"])
    sheet['G8'] = ":"+str(basic_details["Software"]["Test Start Time"])
    sheet['B9'] = "Fan Motor RPM : " + str(basic_details["Software"]["Fan Motor"]["RPM"] + "   " + "Efficiency. :" + basic_details["Software"]["Fan Motor"]["Efficiency"])+" %"
    sheet['G9'] = ":"+str(basic_details["Software"]["Test Stop Time"])
    sheet['B10'] = "Controller Program Version: "+str(basic_details["Software"]["Controller Program Version"])
    sheet["B40"]="Nozzle dia. : "+str(basic_details["Software"]["Nozzle Diameter"][0])
    sheet["b41"]="No Load Readings"

    sheet["C43"]=""+str(basic_details["Software"]["Assembly Engineer"])
    sheet["G43"]=" : "+str(basic_details["Software"]["QA Engineer"])

    sheet["E57"] =int(basic_details["Software"]["Nozzle Diameter"][0])
    sheet["F57"] =int(basic_details["Software"]["Nozzle Diameter"][0])
    sheet["G57"] =int(basic_details["Software"]["Nozzle Diameter"][0])
    sheet["H57"] =int(basic_details["Software"]["Nozzle Diameter"][0])

    sheet["E35"] = str(basic_details["Software"]["Loading Pressure in Kg/cm2"][0])
    sheet["E36"] = str(basic_details["Software"]["Unloading Pressure in Kg/cm2"][0])

    sheet["E19"] =str(basic_details["Software"]["Motor"]["RPM"])
    sheet["F19"] =str(basic_details["Software"]["Motor"]["RPM"])
    sheet["G19"] =str(basic_details["Software"]["Motor"]["RPM"])
    sheet["H19"] =str(basic_details["Software"]["Motor"]["RPM"])


    sheet["E34"] =str(basic_details["Software"]["Safety Valve Pressure"][0])
    sheet["E38"] =str(basic_details["Software"]["Noise Measurement"])

    #sheet["A36"] = "Motor Output Power in KW corrected at "+str(basic_details["actual_RPM"]) +" RPM"
    #sheet["A30"] ="FAD Corrected with " +str(basic_details["actual_RPM"])+ " RPM & Condensate content in m3/min."
    book.save(name)
    book.close()


def post_data_details_auto(reading_1_TS,reading_2_TS,reading_3_TS,reading_4_TS,name="final_format.xlsx"):
    # UPDATE EXCEL
    book = openpyxl.load_workbook(name)
    sheet = book["Internal"]
    
    sheet["E13"] = str(reading_1_TS["Reading Time"])
    sheet["F13"] = str(reading_2_TS["Reading Time"])
    sheet["G13"] = str(reading_3_TS["Reading Time"])
    sheet["H13"] = str(reading_4_TS["Reading Time"])

    sheet["E14"] = str(reading_1_TS["Atmospheric pressure in mm of Hg."])
    sheet["F14"] = str(reading_2_TS["Atmospheric pressure in mm of Hg."])
    sheet["G14"] = str(reading_3_TS["Atmospheric pressure in mm of Hg."])
    sheet["H14"] = str(reading_4_TS["Atmospheric pressure in mm of Hg."])

    sheet["E15"] = str(reading_1_TS["Atmospheric temp.0C"])
    sheet["F15"] = str(reading_2_TS["Atmospheric temp.0C"])
    sheet["G15"] = str(reading_3_TS["Atmospheric temp.0C"])
    sheet["H15"] = str(reading_4_TS["Atmospheric temp.0C"])

    sheet["E16"] = str(reading_1_TS["Air Delivery Pressure in Kg/cm2"])
    sheet["F16"] = str(reading_2_TS["Air Delivery Pressure in Kg/cm2"])
    sheet["G16"] = str(reading_3_TS["Air Delivery Pressure in Kg/cm2"])
    sheet["H16"] = str(reading_4_TS["Air Delivery Pressure in Kg/cm2"])

    sheet["E17"] = str(reading_1_TS["Compresspr Oil Temperature 0C"])
    sheet["F17"] = str(reading_2_TS["Compresspr Oil Temperature 0C"])
    sheet["G17"] = str(reading_3_TS["Compresspr Oil Temperature 0C"])
    sheet["H17"] = str(reading_4_TS["Compresspr Oil Temperature 0C"])

    sheet["E18"] = str(reading_1_TS["Compressor Oil Pressure Kg/cm2"])
    sheet["F18"] = str(reading_2_TS["Compressor Oil Pressure Kg/cm2"])
    sheet["G18"] = str(reading_3_TS["Compressor Oil Pressure Kg/cm2"])
    sheet["H18"] = str(reading_4_TS["Compressor Oil Pressure Kg/cm2"])

    sheet["E20"] = str(reading_1_TS["Air Inlet Temp.0C"])
    sheet["F20"] = str(reading_2_TS["Air Inlet Temp.0C"])
    sheet["G20"] = str(reading_3_TS["Air Inlet Temp.0C"])
    sheet["H20"] = str(reading_4_TS["Air Inlet Temp.0C"])

    sheet["E21"] = str(reading_1_TS["Air Outlet Temp. at Nozzle 0C"])
    sheet["F21"] = str(reading_2_TS["Air Outlet Temp. at Nozzle 0C"])
    sheet["G21"] = str(reading_3_TS["Air Outlet Temp. at Nozzle 0C"])
    sheet["H21"] = str(reading_4_TS["Air Outlet Temp. at Nozzle 0C"])

    sheet["E22"] = str(reading_1_TS["DPT_500"])
    sheet["F22"] = str(reading_2_TS["DPT_500"])
    sheet["G22"] = str(reading_3_TS["DPT_500"])
    sheet["H22"] = str(reading_4_TS["DPT_500"])

    sheet["E23"] = str(reading_1_TS["DPT_1000"])
    sheet["F23"] = str(reading_2_TS["DPT_1000"])
    sheet["G23"] = str(reading_3_TS["DPT_1000"])
    sheet["H23"] = str(reading_4_TS["DPT_1000"])

    sheet["E22"] = str(reading_1_TS["DPT_500"])
    sheet["F22"] = str(reading_2_TS["DPT_500"])
    sheet["G22"] = str(reading_3_TS["DPT_500"])
    sheet["H22"] = str(reading_4_TS["DPT_500"])


    sheet["E29"] = str(reading_1_TS["Voltage"])
    sheet["F29"] = str(reading_2_TS["Voltage"])
    sheet["G29"] = str(reading_3_TS["Voltage"])
    sheet["H29"] = str(reading_4_TS["Voltage"])

    sheet["E30"] = str(reading_1_TS["Current"])
    sheet["F30"] = str(reading_2_TS["Current"])
    sheet["G30"] = str(reading_3_TS["Current"])
    sheet["H30"] = str(reading_4_TS["Current"])

    sheet["E31"] = str(reading_1_TS["Power"])
    sheet["F31"] = str(reading_2_TS["Power"])
    sheet["G31"] = str(reading_3_TS["Power"])
    sheet["H31"] = str(reading_4_TS["Power"])

    sheet["E37"] = str(reading_1_TS["Relative Humidity"])
    sheet["F37"] = str(reading_2_TS["Relative Humidity"])
    sheet["G37"] = str(reading_3_TS["Relative Humidity"])
    sheet["H37"] = str(reading_4_TS["Relative Humidity"])

    sheet["E34"] = str(reading_4_TS["Safety Valve Qty"])

    book.save(name)
    book.close()


'''
basic_details = {'compressor_model': 'KES-7', 'compressor_series': 'Maxima IE3', 'compressor_pressure': '7', 'suction_drop': '', 'motor_rpm': '', 'transaction_details': 'test_start', 'motor_make': '', 'motor_kw': '', 'motor_srno': '', 'motor_eff': '', 'fan_motor_make': '', 'fan_motor_kw': '', 'fan_motor_srno': '', 'fan_motor_rpm': '', 'fan_motor_eff': '',
                 'oa_number': '', 'sbm_number': '', 'test_date': '', 'test_start_time': '', 'test_stop_time': '', 'test_duration': '180', 'test_id': '', 'assembly_incharge': '', 'qa_incharge': '', 'actual_operating_pressure': '', 'compressor_serial_no': '', 'ambient_temperature': '31', 'ambient_pressure': '720.1', 'ambient_humidity': '62', 'actual_RPM': ''}
model_details = {'item_count': 0, 'items': [{'Model': '', 'Kw': "", 'Pressure': "", 'Safety Valve Pressure': "", ' Std FAD in CFM': "", 'Std  FAD in m3/min': "", ' Min FAD in CFM': "", ' Max FAD in CFM': "", 'Tolerance for CFM in %': "",
                                             'Std Normal Shaft Power (Load)(kw/CFM)': "", 'Min Normal Shaft Power (Load)(kw/CFM)': "", 'Max Normal Shaft Power (Load)(kw/CFM)': "", 'Tolerance for Power (Load)in %': "", 'Normal Power (Unload)': 'NA', 'Tolerance for Power (Unload) in %': "", 'Loading Pressure': "", 'Unloading Pressure': "", 'Operating Pressure': "", 'Nozzle Size': "", 'Series': ''}]}
'''
#import pymongo
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#kpcl_testbed = myclient["KpclTestbed"]
#test = kpcl_testbed["test"]



#basic_details = test.find_one({},{"_id":0})

#update_basic_details_auto(basic_details,name="final_format.xlsx")
#update_model_details_auto(model_details,name="final_format.xlsx")


def excel_to_pdf():
    os.system("cp final_format.xlsx Latest_report.xlsx")
    try:
        os.system("soffice --headless --convert-to pdf Latest_report.xlsx")
    except:
        os.system("libreoffice --headless --convert-to pdf Latest_report.xlsx")