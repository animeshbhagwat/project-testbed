from flask import Flask, render_template, send_file, request
from flask_socketio import SocketIO, emit
import pymongo
from bson.objectid import ObjectId
import copy
from flask_login import login_required

import openpyxl
import time
from datetime import datetime
import database_operations
import excel_operations
# import new_test       commented
import os

# ----------------------------
from pymodbus.client.sync import ModbusSerialClient as Modbusclient
import time
import math
from ctypes import *
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret1!'
socketio = SocketIO(app)
thread = None

# -----------GLOBALS----------------------
session_details = {}
global model_details
global basic_details
global test_in_process
global test_status_color
global test_instructions
global measurable_params
test_instructions = "Please Click Start to Continue"
test_status_color = 'red'
test_in_process = 0
basic_details = {'compressor_model': 'KES-7', 'compressor_series': 'Maxima IE3', 'compressor_pressure': '7', 'suction_drop': '', 'motor_rpm': '', 'transaction_details': 'test_start', 'motor_make': '', 'motor_kw': '', 'motor_srno': '', 'motor_eff': '', 'fan_motor_make': '', 'fan_motor_kw': '', 'fan_motor_srno': '', 'fan_motor_rpm': '', 'fan_motor_eff': '',
                 'oa_number': '', 'sbm_number': '', 'test_date': '', 'test_start_time': '', 'test_stop_time': '', 'test_duration': '180', 'test_id': '', 'assembly_incharge': '', 'qa_incharge': '', 'actual_operating_pressure': '', 'compressor_serial_no': '', 'ambient_temperature': '31', 'ambient_pressure': '720.1', 'ambient_humidity': '62', 'actual_RPM': ''}
model_details = {'item_count': 0, 'items': [{'Model': '', 'Kw': "", 'Pressure': "", 'Safety Valve Pressure': "", ' Std FAD in CFM': "", 'Std  FAD in m3/min': "", ' Min FAD in CFM': "", ' Max FAD in CFM': "", 'Tolerance for CFM in %': "",
                                             'Std Normal Shaft Power (Load)(kw/CFM)': "", 'Min Normal Shaft Power (Load)(kw/CFM)': "", 'Max Normal Shaft Power (Load)(kw/CFM)': "", 'Tolerance for Power (Load)in %': "", 'Normal Power (Unload)': 'NA', 'Tolerance for Power (Unload) in %': "", 'Loading Pressure': "", 'Unloading Pressure': "", 'Operating Pressure': "", 'Nozzle Size': "", 'Series': ''}]}
measurable_params = {
    'compressor_air_oil_temp': "",
    'compressor_air_oil_press': "",
    'discharge_pressure': "",
    'upstream_pressure': "",
    'downstream_pressure': "",
    'current': "",
    'voltage': "",
    'power': "",
    'FAD': "",
    'condensate': "",
    "air_inlet_temp": ""}


@socketio.on('start_test')
def start_details_function(msg):
    print("========================Received Start Test Command=========================")
    print(msg)
    global test_in_process
    global test_status_color
    global basic_details
    global model_details
    global measurable_params
    test_status_color = "green"
    if test_in_process == 0:
        test_in_process = 1
        socketio.emit("test_status", test_status_color)
        now = datetime.now()
        basic_details["test_date"] = now.strftime("%d/%m/%Y")
        basic_details["test_start_time"] = now.strftime("%H:%M")
        basic_details["test_id"] = "R20128"
        os.system("cp template_pdf/R2_parameters.xlsx temp.xlsx")
        excel_operations.update_basic_details(
            basic_details=basic_details, name="temp.xlsx")
        excel_operations.update_model_details(
            model_details=model_details, name="temp.xlsx")
        os.system("soffice --headless --convert-to pdf temp.xlsx")
        os.system("sudo cp temp.pdf /var/www/html")
        global test_instructions
        test_instructions = "Test is in progress.Please stop this test to continue."
        socketio.emit("test_start_details",
                      {"test_id": basic_details['test_id'], "test_start_time": basic_details["test_start_time"],
                       "test_date": basic_details["test_date"], "test_status_instructions": test_instructions})
        thread = socketio.start_background_task(target=new_test.live2_thread(
            msg, socketio, basic_details, model_details['items'][0], measurable_params))


@socketio.on('pause_test')
def pause_details_function(msg):
    print("========================Received Pause Test Command=========================")
    print(msg)
    global test_in_process
    global test_status_color
    test_status_color = "orange"
    if test_in_process == 1:
        socketio.emit("test_status", test_status_color)
        excel_operations.update_basic_details(
            basic_details=basic_details, name="temp.xlsx")


@socketio.on('stop_test')
def stop_details_function(msg):
    print("========================Received Stop Test Command=========================")
    print(msg)
    global test_in_process
    global basic_details
    if test_in_process == 1:
        test_in_process = 0
        global test_status_color
        test_status_color = "red"
        socketio.emit("test_status", test_status_color)
        now = datetime.now()
        basic_details["test_stop_time"] = now.strftime("%H:%M")
        excel_operations.update_basic_details(
            basic_details=basic_details, name="temp.xlsx")
        os.system("soffice --headless --convert-to pdf temp.xlsx")
        os.system("sudo cp temp.pdf /var/www/html")
        socketio.emit("test_stop_details", {
                      "test_stop_time": basic_details['test_stop_time']})
        new_test.client.write_register(461, 0b0000000000000000, unit=10)
        time.sleep(2)


@socketio.on('live1_load')
def live1_load_function(msg):
    print("======================== Live 1 Load =========================")
    print(msg)
    global model_details
    global basic_details
    socketio.emit('model_details', model_details['items'][0])
    print("Emmitted model details")
    socketio.emit('basic_details', basic_details)
    print(basic_details)
    print("Emmitted basic details")
    socketio.emit("test_status", test_status_color)
    global test_instructions
    socketio.emit("test_start_details",
                  {"test_id": basic_details['test_id'], "test_start_time": basic_details["test_start_time"],
                   "test_date": basic_details["test_date"], "test_status_instructions": test_instructions})


@socketio.on('live2_load')
def live2_load_function(msg):
    print("======================== Live 2 Load =========================")
    print(msg)
    global test_status_color
    socketio.emit("test_status", test_status_color)
    global measurable_params
    socketio.emit("measurable_params", measurable_params)
    global test_in_process
    socketio.emit("live2_data_progress", test_in_process)
    socketio.emit('test_position', {
        "test_position": new_test.test_position})


@socketio.on('live3_load')
def live3_load_function(msg):
    print("======================== Live 3 Load =========================")
    print(msg)
    global test_status_color
    socketio.emit("test_status", test_status_color)
    global measurable_params
    socketio.emit("measurable_params", measurable_params)
    global test_in_process
    socketio.emit("live2_data_progress", test_in_process)


@socketio.on('live34_load')
def live3_load_function(msg):
    print("======================== Live 34 Load =========================")
    print(msg)
    global test_status_color
    socketio.emit("test_status", test_status_color)


@socketio.on('basic_details')
def basic_details_function(msg):
    print("======================== Basic Details Function =========================")
    print(msg)
    global model_details
    global basic_details
    basic_details = copy.deepcopy(msg)
    '''
    model_details = database_operations.return_model_details(name='Model Details1.xlsx',sheet='Compiled_Data',model=basic_details['compressor_model'],
                                                    pressure=float(basic_details['compressor_pressure']),
                                                    series=basic_details['compressor_series'])
    print(model_details)
    if model_details['item_count'] !=0 :
        socketio.emit('model_details', model_details['items'][0])
    '''
# ------------------------Input from form to update kpcl_testbed Data-Base with Collection test-----
@socketio.on("form_data")
def form_data(data):    
    print("======================== Form Data Details Function =========================")
    print(data)
    response = database_operations.kpcl_database(data)
    if response != 0:
        socketio.emit("Test_Specification_Details", response)
        print("Successfully Created !!")
    else:
        socketio.emit("Test_Specification_Details", 0)

# ----------------------------------------------------------------------------------------------------------------------------#
# ^This Function Store the Form data to Data-Base as well as It update the Front end live1.html "Test Specification Details" #
# Table Data.                                                                                                                #
# ----------------------------------------------------------------------------------------------------------------------------#


@app.route('/')
def dummy_data():
    return render_template('main.html')


@app.route('/welcome.html')
def index():
    return render_template('welcome.html')


@app.route('/login.html')
def login():
    return render_template('login.html')


@app.route('/table-datatable3.html')
@login_required
def datatable3():
    return render_template('table-datatable3.html')


@app.route('/live1.html')
def live1_class():
    return render_template('live1.html')


@app.route('/live2.html')
def live2():
    return render_template('live2.html')


@app.route('/live3.html')
def live3():
    return render_template('live3.html')


@app.route('/live4.html')
def live4():
    return render_template('live4.html')


@app.route('/live5.html')
def live5():
    return render_template('live5.html')


@app.route('/live7.html')
def live7():
    return render_template('live7.html')


@app.route('/live8.html')
def live8():
    return render_template('live8.html')


@app.route('/dashboard.html')
def dashboard():
    return render_template('dashboard.html')


@app.route('/testbed1.html')
def testbed1():
    return render_template('testbed1.html')


@app.route('/cal_cert.html')
def cal_cert():
    return render_template('cal_cert.html')


@app.route('/r_table.html')
def r_table():
    return render_template('r_table.html')


@app.route('/model.html')
def model():
    return render_template('model.html')


@app.route('/profile.html')
def profile():
    return render_template('profile.html')


@app.route('/contact.html')
def contact():
    return render_template('contact.html')


@app.route('/testdetails')
def testdetails_class():
    return render_template('testdetails.html')


@app.route('/livereadings')
def livereadings_class():
    return render_template('livereadings.html')


@app.route('/flow')
def flow_class():
    return render_template('flow.html')


@app.route('/analytics1')
def analytics_class1():
    return render_template('analytics1.html')


@app.route('/pdftrial')
def return_files_tut():
    try:
        return send_file('temp.pdf', attachment_filename='temp.pdf')
    except Exception as e:
        return str(e)


@app.route('/getfromserver')
def return_files_tut1():
    try:
        return send_file('server.pdf', attachment_filename='server.pdf')
    except Exception as e:
        return str(e)


if __name__ == '__main__':
    socketio.run(app, debug=True, host='0.0.0.0', port=5000)
