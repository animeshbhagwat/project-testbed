import pymongo
import pandas
import numpy as np
import pymongo


def return_model_details(model,pressure,series):
    excel_data_df = pandas.read_excel('Model Details1.xlsx', sheet_name='Compiled_Data')
    '''print(excel_data_df.columns.ravel())
    print("---------------------------------")
    print(excel_data_df.Model.unique())
    print("---------------------------------")
    print(excel_data_df.Series.unique())
    print("---------------------------------")
    print(excel_data_df.Pressure.unique())'''

    dict_data = excel_data_df.to_dict(orient='records')
    # =============================================================================================
    print("======================================================================")
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["model_database"]
    mycol = mydb["screw_compressor"]
    mycol.drop()
    mycol = mydb["screw_compressor"]
    x = mycol.insert_many(dict_data)
    myquery = {"$and":[{"Model":model},{'Pressure':pressure},{'Series':series}]}
    mydoc = mycol.find(myquery,{"_id":0})

    all_possibilites = {'item_count':0,'items':[]}
    for x in mydoc:
        print(x)
        all_possibilites['items'].append(x)
        all_possibilites['item_count'] += 1
    print(all_possibilites)
    return all_possibilites



print(return_model_details("KES-7",float('13'),"Prima Series IE2 Motor"))



