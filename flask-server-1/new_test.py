from flask import Flask, render_template,send_file
from flask_socketio import SocketIO, emit
import pymongo
import openpyxl
from datetime import datetime
import os
from bson.objectid import ObjectId
#------------------------------
# LOCAL DATABASE INITIALIZE
#------------------------------
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["kpcl"]
joblist_db = mydb["testbed1"]
joblist_db.drop()
#---------------------
#Queries
#---------------------
def return_job_serial_nos():
    serial_number =[]
    print("------------------------------")
    for x in joblist_db.find({}, {"_id": 1}):
        serial_number.append(str(x["_id"]))
        print(x["_id"])
    print("------------------------------")
    serial_dict ={"serial_nos":serial_number}
    return  serial_dict
def return_job_details(serial_id):
    myquery = {"_id": ObjectId(serial_id)}

    mydoc = joblist_db.find(myquery)
    for x in mydoc:
        print(x)
    return x
def return_job_details_jobtype(jobtype):
    myquery = {"jobtype": jobtype}

    mydoc = joblist_db.find(myquery).sort("_id",pymongo.ASCENDING)
    all_x=[]
    for x in mydoc:
        #print(x)
        x["_id"]=str(x["_id"])
        all_x.append(x)
    return  all_x
#----------------------------
from pymodbus.client.sync import ModbusSerialClient as Modbusclient
import time
import math
from ctypes import *
import RPi.GPIO as GPIO
import time
import time
import Adafruit_ADS1x15

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder





global client1
global client

global test_position
test_position =""
from bson import ObjectId



def live2_thread(msg,socketio,basic_details,model_details,measurable_params):
    global  test_position
    print("----------------------------IN LIVE1 THREAD------------------------------")
    print(msg)
    socketio.emit('message',{'Success':"Mine"})



    nozzle_constant = 73.39
    ###test_time = 500
    test_time = float(basic_details['test_duration'])*60
    '''reading1_time = 100
    reading2_time = 250
    reading3_time = 450
    '''
    reading1_time = test_time *0.3
    reading2_time = test_time *0.6
    reading3_time = test_time *0.85
    reading4_time = test_time * 0.95

    # Global Variables
    col = 'C'
    comp_air_oil_temp = 100
    comp_air_oil_pressure = 101
    dpt1 = 450
    dpt2 = 1000
    voltage = 28
    current = 50
    power = 300
    temp_out = 30
    # ---------------------------
    '''loading_pressure = 13
    unloading_pressure = 12
    safety_valve_pressure = 14.5'''
    loading_pressure = float(model_details['Loading Pressure'])
    unloading_pressure = float(model_details['Unloading Pressure'])
    safety_valve_pressure = float(model_details['Safety Valve Pressure'])
    operating_pressure = float(basic_details['actual_operating_pressure'])


    def convert(s):
        i = int(s, 16)  # convert from hex to a Python int
        cp = pointer(c_int(i))  # make this into a c integer
        fp = cast(cp, POINTER(c_float))  # cast the int pointer to a float pointer
        return fp.contents.value  # dereference the pointer, get the float
    def take_readings():
        global client
        global client1

        var1 = 500
        result = [100, 100]

        value = client.read_holding_registers(503, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(var)
        var = convert(var)
        var1 = round(float(var) * 1, 3)

        print("AOS Tank Temp : " + str(var1))
        comp_air_oil_temp = var1
        measurable_params['compressor_air_oil_temp'] = str(var1)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"AOS Tank Temp": str(var1)})

        time.sleep(1)

        value = client.read_holding_registers(495, 2, unit=10)

        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("AOS Tank Pressure : " + str(var1))
        measurable_params['compressor_air_oil_press'] = str(var1)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"AOS Tank Pressure": str(var1)})
        comp_air_oil_pressure = var1



        time.sleep(3)

        print("Air Inlet Temp : " + str(32))
        measurable_params["air_inlet_temp"] = str(32)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"Air Inlet Temp": str(32)})


        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)
        discharge_press = var1
        print("Discharge Pressure : " + str(var1))
        measurable_params['discharge_pressure'] = str(var1)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        time.sleep(1)

        value = client.read_holding_registers(478, 1, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        # var=hex(result[0])
        # var=var.replace("0x","")
        # print(type(var))
        # var = convert(var)
        var1 = round(var, 3)

        print("Diffrential Pressure : " + str(result[0] / 10))

        socketio.emit('message', {"Differential pressure": str(round(result[0] / 10,3))})

        time.sleep(1)

        value = client.read_holding_registers(479, 1, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        # var=hex(result[0])
        # var=var.replace("0x","")
        # print(type(var))
        # var = convert(var)
        var1 = round(var, 3)

        print("Unload Pressure : " + str(result[0] / 10))
        socketio.emit('message', {"Unload Pressure":  str(round(result[0] / 10,3))})

        time.sleep(1)

        print("----------------------------------\n")

        value = client1.read_holding_registers(3908, 2, unit=0x01)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(var)
        var = convert(var)
        var1 = round(float(var) * 1, 3)

        print("Voltage :" + str(var1))
        measurable_params['voltage'] = str(var1)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"Voltage": str(var1)})
        voltage = var1

        time.sleep(1)

        value = client1.read_holding_registers(3902, 2, unit=0x01)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(var)
        var = convert(var)
        var1 = round(float(var) *0.001, 3)

        print("Power :" + str(var1))
        measurable_params['power'] = str(var1)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"Power": str(var1)})
        power = var1
        time.sleep(1)

        value = client1.read_holding_registers(3912, 2, unit=0x01)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(var)
        var = convert(var)
        var1 = round(float(var) * 1, 3)

        print("Current :" + str(var1))
        measurable_params['current'] = str(var1)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"Current": str(var1)})
        current = var1
        print("----------------------------------\n")

        temp = [0] * 8
        dpt500 = 200
        dpt1000 = 1000
        temp1 = 50
        # measure dpt1
        GPIO.output(dpt1_ad, GPIO.HIGH)
        GPIO.output(dpt2_ad, GPIO.LOW)
        GPIO.output(pt100_ad, GPIO.LOW)

        time.sleep(1)
        for i in range(0, 8):
            temp[i] = ((adc.read_adc(2, gain=GAIN) / 32767 * 6.144) - 1.02) * 125
        dpt500 = round(sum(temp) / 8,3)
        print("DPT1 :" + str(dpt500))
        measurable_params['upstream_pressure'] = str(dpt500)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"DPT1": str(dpt500)})
        dpt1 = dpt500
        time.sleep(1)
        temp = [0] * 8
        # measure dpt2
        GPIO.output(dpt1_ad, GPIO.LOW)
        GPIO.output(dpt2_ad, GPIO.HIGH)
        GPIO.output(pt100_ad, GPIO.LOW)

        time.sleep(1)
        for i in range(0, 8):
            temp[i] = ((adc.read_adc(2, gain=GAIN) / 32767 * 6.144) - 1.01) * 250
        dpt1000 = round(sum(temp) / 8,3)
        print("DPT2 :" + str(dpt1000))
        measurable_params['downstream_pressure'] = str(dpt1000)
        socketio.emit("measurable_params", measurable_params)

        socketio.emit('message', {"DPT2": str(dpt1000)})
        dpt2 = dpt1000

        time.sleep(1)
        temp = [0] * 8
        # measure pt100
        GPIO.output(dpt1_ad, GPIO.LOW)
        GPIO.output(dpt2_ad, GPIO.LOW)
        GPIO.output(pt100_ad, GPIO.HIGH)

        time.sleep(1)
        for i in range(0, 8):
            temp[i] = ((adc.read_adc(2, gain=GAIN) / 32767 * 6.144) - 2.3) / 0.006975
        temp1 = round(sum(temp) / 8,3)
        print("TEMP :" + str(temp1))
        measurable_params["air_inlet_temp"] = str(temp1)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"TEMP": str(temp1)})
        temp_out=temp1
        time.sleep(1)

        print("-----------------------------\n")
        FAD = 5
        ambient_temp = 29
        ambient_pressure = 720
        ambient_humidity = 65
        suction_filter_drop = 0

        upstream_press = dpt500
        downstream_press = dpt1000
        air_outlet_temp = temp1
        air_compressor_delivery_pressure_outlet = 6.6

        sid = (upstream_press + ambient_pressure * 13.52) * downstream_press / (273 + air_outlet_temp)

        print("sid " + str(sid))
        FAD = "NA"
        if sid > 0:
            FAD = nozzle_constant * ((ambient_temp + 273) / (ambient_pressure * 13.52 - suction_filter_drop)) * \
                  (math.sqrt(sid)) / 60
        FAD = round(FAD,3)
        print("FAD :" + str(FAD))
        measurable_params["FAD"] = str(FAD)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"FAD": str(FAD)})

        print("-----------------------------\n")
        time.sleep(2)
        condensate = 0.08
        print("Condensate :" + str(condensate))
        measurable_params["condensate"] = str(condensate)
        socketio.emit("measurable_params", measurable_params)
        socketio.emit('message', {"condensate": str(condensate)})
        return measurable_params
    def generate_report(col,measurable_params):
        book = openpyxl.load_workbook("temp.xlsx")
        sheet = book["Internal"]

        now = datetime.now()
        current_time = now.strftime("%H:%M")
        print("Current Time =", current_time)
        sheet[col + '12'] = current_time
        sheet[col + '16'] = measurable_params['compressor_air_oil_temp']
        sheet[col + '17'] = measurable_params['compressor_air_oil_press']
        sheet[col + '25'] = measurable_params['downstream_pressure']
        sheet[col + '26'] = measurable_params['upstream_pressure']
        sheet[col + '32'] = measurable_params['voltage']
        sheet[col + '33'] = measurable_params["current"]
        sheet[col + '34'] = measurable_params['power']
        sheet[col + '24'] = measurable_params["air_inlet_temp"]
        # constants
        #sheet[col + '38'] = loading_pressure
        #sheet[col + '39'] = unloading_pressure
        # constants
        #sheet['C37'] = safety_valve_pressure

        book.save('temp.xlsx')
        book.close()

    '''==========================================================================================================='''
    '''================================================SETTINGS=================================================='''
    '''==========================================================================================================='''
    socketio.emit('test_position', {"test_position": "Writing Initial Setting and Starting the Compressor"})
    print("Writing Initial Setting and Starting the Compressor")
    test_position = "Writing Initial Setting and Starting the Compressor"

    # Compressor Air Oil Temp : 40504
    # Compressor Oil Pressure : 40496
    # Unload Pressure :40480
    # Load Pressure : 40481

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    dpt1_ad = 17
    dpt2_ad = 27
    pt100_ad = 22

    GPIO.setup(dpt1_ad, GPIO.OUT)
    GPIO.setup(dpt2_ad, GPIO.OUT)
    GPIO.setup(pt100_ad, GPIO.OUT)

    GPIO.setup(10, GPIO.OUT)
    GPIO.setup(9, GPIO.OUT)
    GPIO.setup(11, GPIO.OUT)
    GPIO.setup(5, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)

    GPIO.setup(dpt1_ad, GPIO.OUT)
    GPIO.setup(dpt2_ad, GPIO.OUT)
    GPIO.setup(pt100_ad, GPIO.OUT)

    GPIO.setup(10, GPIO.HIGH)
    GPIO.setup(9, GPIO.HIGH)
    GPIO.setup(11, GPIO.HIGH)
    GPIO.setup(5, GPIO.HIGH)
    GPIO.setup(6, GPIO.HIGH)

    adc = Adafruit_ADS1x15.ADS1115()
    GAIN = 2 / 3

    # measure dpt1
    # GPIO.output(dpt1_ad,GPIO.HIGH)
    # GPIO.output(dpt2_ad,GPIO.LOW)
    # GPIO.output(pt100_ad,GPIO.LOW)

    # measure dpt2
    # GPIO.output(dpt1_ad,GPIO.LOW)
    # GPIO.output(dpt2_ad,GPIO.HIGH)
    # GPIO.output(pt100_ad,GPIO.LOW)

    # measure pt100
    # GPIO.output(dpt1_ad,GPIO.LOW)
    # GPIO.output(dpt2_ad,GPIO.LOW)
    # GPIO.output(pt100_ad,GPIO.HIGH)

    GPIO.output(dpt1_ad, GPIO.LOW)
    GPIO.output(dpt2_ad, GPIO.LOW)
    GPIO.output(pt100_ad, GPIO.LOW)

    time.sleep(1)

    temp = [0] * 8

    # client = system microcontroller
    # client1 = mfm
    global client
    global client1
    client = Modbusclient(method='rtu', port='/dev/ttyUSB1', stopbits=1, bytesize=8, parity='N', baudrate=9600,
                          timeout=0.3)

    connection = client.connect()
    print(connection)

    client1 = Modbusclient(method='rtu', port='/dev/ttyUSB0', stopbits=1, bytesize=8, parity='E', baudrate=19200,
                           timeout=0.3)

    connection = client1.connect()
    print(connection)

    '''sid = client.write_register( 456,7, unit=10)
    time.sleep(2)
    print(sid)
    value = client.read_holding_registers(455,12,unit=10)
    #print('[{}]'.format(', '.join(hex(x) for x in value.registers)))
    print(value.registers)
    #sid=client.write_register(480, 0b0000000000001111)
    #print(sid)'''


    #socketio.emit('message', {"Message": "Set Unload to 12.5"})
    temp = "Set Unload Pressure to "+ str(unloading_pressure)
    socketio.emit('message', {"Message": temp})

    # enable write
    sid = client.write_register(461, 0b1000000000000000, unit=10)
    time.sleep(2)
    print(sid)

    # set unload to unloading pressure
    sid1 = client.write_register(479, int(unloading_pressure*10), unit=10)
    time.sleep(2)
    print(sid1)

    # turn on
    socketio.emit('message', {"Message": "Turning on"})

    sid = client.write_register(461, 0b1010000000000000, unit=10)
    time.sleep(2)
    print(sid)

    '''==========================================================================================================='''
    '''============================================DISCHARGE PRES > OPERATING PRES ==============================='''
    '''==========================================================================================================='''
    socketio.emit('test_position', {"test_position": "Waiting for Discharge Pressure to be greater than Operating Pressure"})
    print("Waiting for Discharge Pressure to be greater than Operating Pressure")
    test_position = "Waiting for Discharge Pressure to be greater than Operating Pressure"
    # wait for discharge pressure to be greater than 7,0
    #socketio.emit('message', {"Message": "Waiting for discharge pressure to be greater than Unload Pressure"})
    socketio.emit('message', {"Message": "Waiting for discharge pressure to be greater than Operating Pressure"})
    var1 = 0

    '''while var1 < unload_pressure:'''
    while var1 < operating_pressure :
        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        time.sleep(1)


    '''==========================================================================================================='''
    '''============================================READINGS====================================================='''
    '''==========================================================================================================='''
    socketio.emit('test_position', {"test_position": "Starting the readings."})
    print("Starting the readings.")
    test_position = "Taking the readings"
    # begin readings
    start_time = time.time()
    while time.time() - start_time < test_time :
        measurable_params=take_readings()
        time_past = time.time() - start_time
        if ( time_past < reading1_time):
            col='C'
            generate_report(col,measurable_params )
            os.system("soffice --headless --convert-to pdf temp.xlsx")
            os.system("sudo cp temp.pdf /var/www/html")
            print("Successfully updated values 0")
        if (time_past > reading1_time and time_past < reading2_time ):
            col = 'D'
            generate_report(col, measurable_params)
            os.system("soffice --headless --convert-to pdf temp.xlsx")
            os.system("sudo cp temp.pdf /var/www/html")
            print("Successfully updated values 1")
        if (time_past > reading2_time and time_past < reading3_time):
            col = 'E'
            generate_report(col, measurable_params)
            os.system("soffice --headless --convert-to pdf temp.xlsx")
            os.system("sudo cp temp.pdf /var/www/html")
            print("Successfully updated values 2")
        if (time_past > reading3_time and time_past < reading4_time):
            col = 'F'
            generate_report(col, measurable_params)
            os.system("soffice --headless --convert-to pdf temp.xlsx")
            os.system("sudo cp temp.pdf /var/www/html")
            print("Successfully updated values 3")
        time.sleep(1)


    '''==========================================================================================================='''
    '''===============================================NO POWER READING============================================'''
    '''==========================================================================================================='''
    socketio.emit('test_position', {"test_position": "Taking No Load Readings.Please check if pressure is 0."})
    print("Taking No Load Readings.Please check if pressure is 0.")
    test_position =  "Taking No Load Readings.Please check if pressure is 0."
    print("Unloading Machine")
    socketio.emit('message', {"Message": "Unloading Machine"})

    # unload
    sid = client.write_register(461, 0b1110000000000000, unit=10)
    time.sleep(2)
    print(sid)

    var1 = 8.0
    # wait till discharge pressure is 0
    socketio.emit('message', {"Message": "Wait till discharge pressure is 0."})

    while var1 > 0.5:
        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        time.sleep(1)

    print("No Load Reading")
    socketio.emit('message', {"Message": "Taking No Load Reading"})

    # take power reading --no load power
    measurable_params = take_readings()
    #update no load readings
    book = openpyxl.load_workbook("temp.xlsx")
    sheet = book["Internal"]
    sheet['C43'] = measurable_params['power']
    sheet['E43'] = measurable_params['current']
    sheet['G43'] = measurable_params['voltage']
    sheet['D42'] = var1
    sheet['G42'] = measurable_params['compressor_air_oil_press']
    book.save('temp.xlsx')
    book.close()
    os.system("soffice --headless --convert-to pdf temp.xlsx")
    os.system("sudo cp temp.pdf /var/www/html")

    '''==========================================================================================================='''
    '''==============================================SAFETY VALVE TEST============================================'''
    '''==========================================================================================================='''

    socketio.emit('test_position', {"test_position": "Starting Safety Valve Test.Please Increase Pressure to be more than Safety Valve Pressure."})
    print("Starting Safety Valve Test.Please Increase Pressure to be more than Safety Valve Pressure.")
    test_position = "Starting Safety Valve Test.Please Increase Pressure to be more than Safety Valve Pressure."
    print("Set Unload Pressure to greater than sfp")
    temp = "Set Unload Pressure to "+str(safety_valve_pressure + 0.2)
    socketio.emit('message', {"Message": temp})

    # set unload to 7,5
    sid1 = client.write_register(479, int((safety_valve_pressure + 0.2)*10), unit=10)
    time.sleep(2)
    print(sid1)

    print("Loading Machine")
    socketio.emit('message', {"Message": "Wait till discharge pressure is 0."})

    # unload
    sid = client.write_register(461, 0b1010000000000000, unit=10)
    time.sleep(2)
    print(sid)

    # wait till discharge pressure is 0
    while var1 < (safety_valve_pressure - 0.2):
        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        time.sleep(1)

    print("Started Safety valve Test")
    socketio.emit('message', {"Message": "Started Safety Valve Test"})

    counter = 0
    # Safety Valve test begin
    while counter < 2:
        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        if var1 < (safety_valve_pressure - 0.5):
            counter = counter + 1
            print("Count Occured")
            time.sleep(2)

        if var1 > (safety_valve_pressure + 0.5):
            sid = client.write_register(461, 0b0000000000000000, unit=10)
            time.sleep(2)
            print(sid)

        time.sleep(0.5)
        time.sleep(1)


    sid = client.write_register(461, 0b0000000000000000, unit=10)
    time.sleep(2)
    os.system("soffice --headless --convert-to pdf temp.xlsx")
    os.system("sudo cp temp.pdf /var/www/html")
    socketio.emit('test_position', {
        "test_position": "Test Done! Please Press stop to exit"})
    print("Test Done! Please Press stop to exit")
    test_position = "Test Done! Please Press stop to exit"
    '''==========================================================================================================='''
    '''==========================================================================================================='''
    '''==========================================================================================================='''



